export interface IUsesFromAuthorityString {
    authority: string;
    facts: string[];
}