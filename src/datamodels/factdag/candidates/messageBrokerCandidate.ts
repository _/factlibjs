import {Namespaces} from "@i5/factlib-utils";
import {FactCandidate} from "./factCandidate";
import {EmptyTrunk} from "../../../factClient/EmptyTrunk";
import {IResourceID} from "../../interfaces/IResourceID";

/**
 * Representation of a MessageBrokerCandidate. <br />
 * See also: [[FactCandidate]] and [[MessageBroker]]
 */
export class MessageBrokerCandidate<TTrunk = EmptyTrunk> extends FactCandidate<TTrunk> {

    constructor(store: any, resourceID: IResourceID, trunk: TTrunk) {
        super(store, resourceID, trunk);
        this.addBrokerProv();
    }


    /**
     * Writes the rdf:type fact:Broker locally to the current Candidate.
     * This should be present for every Broker.
     */
    public addBrokerProv(): void {
        const uri = this.resourceID.toString();
        const elem = this._store.sym(uri);
        this._store.add(elem,
            Namespaces.RDF(Namespaces.RDF_RELATION.TYPE),
            Namespaces.FACT(Namespaces.FACT_TYPE.BROKER),
            elem.doc());
    }

    /**
     * Sets the output type (topic/queue) to the local store.
     * @param type The type (topic/queue) describing how change notifications for LDP resources are published.
     */
    public addOutputType(type: string): void {
        this.addFactRelation(Namespaces.FACT_RELATION.HAS_OUTPUT_TYPE, type);
    }

    /**
     * Sets the name of the output topic/queue in the local store.
     * @param name The name of the topic/queue that change notifications for LDP resources are published to.
     */
    public addOutputName(name: string): void {
        this.addFactRelation(Namespaces.FACT_RELATION.OUTPUTS_TO, name);

    }

    /**
     * Sets the URL of the broker in the local store.
     * @param url The URL needed for a connection to the broker.
     */
    public addURL(url: URL): void {
        const uri = this.resourceID.toString();
        const elem = this._store.sym(uri);
        this._store.add(elem,
            Namespaces.SC(Namespaces.SCHEMA_RELATION.URL),
            url.toString(),
            elem.doc());
    }


}
