export interface MementoLocation {
    readonly date: Date;
    readonly url: URL;
}