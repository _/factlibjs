import {LdpMementoClient} from "../../../index";

const service = new LdpMementoClient();
const factDagResourceFactory = service.getFactDagResourceFactory();
const idFactory = service.getIDFactory();
const dep1 = "http://localhost:8081/";
const dep2 = "http://localhost:8082/";

async function setupEnvironment(authorityID: string,
                                brokerResourceID: string,
                                brokerOutputName: string,
                                brokerOutputType: string,
                                brokerURL: URL): Promise<void> {
    await service.createDirectory(idFactory.createResourceID(authorityID, ".well-known"));
    const brokerCandidate =  factDagResourceFactory.createEmptyBrokerCandidate(idFactory.createResourceID(authorityID, brokerResourceID));
    //new MessageBrokerCandidate("", slug, brokerFactURI);
    brokerCandidate.addOutputName(brokerOutputName);
    brokerCandidate.addOutputType(brokerOutputType);
    brokerCandidate.addURL(brokerURL);
    const broker = await service.createMessageBrokerResource(brokerCandidate);

    const authCandidate = factDagResourceFactory.createEmptyAuthorityCandidate(idFactory.createAuthorityResourceID(authorityID));
    authCandidate.addResposibleBroker(broker);
    const authority = await service.createAuthorityResource(authCandidate);
    await service.createDirectory(idFactory.createResourceID(authority.resourceID.authorityID, "facts"));
    await service.createDirectory(idFactory.createResourceID(authority.resourceID.authorityID, "processes"));
    await service.createDirectory(idFactory.createResourceID(authority.resourceID.authorityID, "activities"));
}

setupEnvironment(dep1, "broker1", "output", "topic", new URL("tcp://localhost:61613"));
setupEnvironment(dep2, "broker2", "output", "topic", new URL("tcp://localhost:61614"));
