#!/bin/bash
printf "{\"commit\": \"%s\"}" $1 > output/commit.json
curl -v -XPATCH -H 'Content-Type: application/sparql-update' -d 'PREFIX SC: <http://schema.org/SoftwareSourceCode#>

DELETE 
{ <'"$2"'>
    SC:codeRepository  ?o .
}
INSERT 
{ <'"$2"'> 
    SC:codeRepository  "'"${1}"'" .
}
WHERE
{ ?s ?p ?o
}' $2
