# Docker Dev-Environment
A `docker-compose` setup for deploying a development environment for process development. 
The setup models two separate authorities, each consisting of a Trellis instance, an ActiveMQ message broker and 
an EventMiddleware that reads the ActivityStream from Trellis and transforms it into messages that processes 
can subscribe to individually.  

## Installation
The environment can be deployed with `docker-compose up -d` on a machine with docker installed. 
The deployment can be configured with the [docker-compose.yml](docker-compose.yml).

Users with access to the Middleware repository can download the Middleware Docker image, from the container registry.
To do so, login before downloading the image or executing docker-compose. 
```
docker login registry.git.rwth-aachen.de
```
The password can also be a personal access token that can be created in the user settings in Gitlab.
The [Gitlab-Documentation](https://git.rwth-aachen.de/help/user/packages/container_registry/index) contains further details. 

Alternatively, the image can be created from source, following the documentation in the
[EventMiddleware](https://git.rwth-aachen.de/a-distributed-data-revisioning-system-for-the-iop/eventmiddleware) project.

## Known Problems
If the Broker is not ready before the associated Trellis instance starts, Trellis will not start successfully. 
If that happens, just restart the Trellis container manually.